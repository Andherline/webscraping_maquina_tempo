import requests
import os
import time
from random import randint
import re
from bs4 import BeautifulSoup as soup



episodios = {}
pasta_destino = r'' # definir pasta destino


def baixa_episodio (sessao, headers):
    print('Teste baixa')
    for nome, link in episodios.items():
        # print(f'nome: {nome}')
        # print(f'link: {link}')
        if (os.path.exists(os.path.join(pasta_destino, f'{nome}.mp3'))):
            print('Arquivo já foi baixado')

        else:
            # substituir caracteres não alfanuméricos por '_'.
            nome_tratado = '_'.join(filter(str.isalnum, nome))
            try:
                req_episodio = sessao.get(link, headers=headers,timeout=120)
                print(f'Código http audio mp3: {req_episodio.status_code}')
                print(f'### Baixando o episódio: {nome_tratado}.mp3 ...')
                with open(os.path.join(pasta_destino, f'{nome_tratado}.mp3'), 'wb') as episodio:
                    episodio.write(req_episodio.content)
                print(f'### Episódio {nome_tratado}.mp3 foi baixado.')
            except Exception:
                print('Teste Alternatica Timeout: 120s')
                time.sleep(200)
                req_episodio = sessao.get(link, headers=headers,timeout=120)
                print(f'Código http audio mp3: {req_episodio.status_code}')
                print(f'### Baixando o episódio: {nome_tratado}.mp3 ...')
                with open(os.path.join(pasta_destino, f'{nome_tratado}.mp3'), 'wb') as episodio:
                    episodio.write(req_episodio.content)
                print(f'### Episódio {nome}.mp3 foi baixado.')


def gerador_paginas_podcast(sessao,headers):
    pagina = ''
    for i in range(1,22):
        req = sessao.get(f'https://ocktock.com.br/category/maquina-do-tempo/page/{i}/',headers=headers,timeout=120)
        if req.status_code == requests.codes.OK:
            maq_tempo_soup = soup(req.text, 'html.parser')
            # print(f'Código da requisição: {req.status_code}')
            for a in maq_tempo_soup.find_all('h2', 'entry-title'):
                 pagina = a.find('a', rel='bookmark', href=True).get('href')
                 # print(pagina)
                 req_podcast = sessao.get(pagina,headers=headers,timeout=120)
                 if req_podcast.status_code == requests.codes.OK:
                    pasta_destino = r'/home/babel_oteca/Documentos/teste_project_python/Maquina_do_Tempo'
                    podcast = soup(req_podcast.text, 'html.parser')
                    # print(f'Código da requisição: {req_podcast.status_code}')
                    nome_episodio = podcast.find('h1', 'entry-title').text
                    tag_audio = podcast.audio
                    # print('teste :{}'.format(tag_audio.get('href')))
                    if tag_audio is None:
                        print(f'None: não foi encotrado o link do podcast\n- {nome_episodio}.')
                    else:
                        link_audio = tag_audio.find('a', href=True)
                        # testar e talvez apagar
                        if link_audio is None:
                            print(f'Tratando episódio {nome_episodio}.')
                            link_episodio = podcast.figure.find('audio',src=True).get('src')
                        else:
                            link_episodio = link_audio.get('href')
                        #print(nome_episodio)
                        # print(link_audio.get('href'))
                        # print(link_episodio)
                        episodios[f'{nome_episodio}'] = link_episodio
                 else:
                    print(f'Erro na requisição: código {req.raise_for_status()}')


if __name__=="__main__":
    headers = {'User-Agent':'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/92.0.4515.131 Safari/537.36'}
    inicio = time.time()
    # headers = {'User-Agent": "Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:68.0) Gecko/20100101 Firefox/68.0'}
    # headers = {'user-agent': 'Python script'}
    s = requests.Session()
    gerador_paginas_podcast(s,headers)
    baixa_episodio (s, headers)
    s.close()
    fim = time.time()
    resultado_tempo = fim - inicio
    print(f'Tempo de execução : {time.strftime("%H:%M:%S", time.gmtime(resultado_tempo))}')
